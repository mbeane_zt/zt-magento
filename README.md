# ZT-Gento

This repository contains Magento Commerce using Docker

## Installation

-  Run `docker run -it --rm -v $(pwd):/app/ -v ~/.composer/:/root/.composer/ magento/magento-cloud-docker-php:7.3-cli-1.1 bash -c "composer install && chown www. /app/"` to install composer packages using a PHP CLI container

-  Run `sudo chown -R username. . ~/.composer` (replace _username_ with your username) to fix Magento permissions if you aren't using MacOS

-  If you wish to start with a pre-installed Magento environment, download the appropriate [tarball](https://bitbucket.org/mbeane_zt/zt-magento/downloads) with the Magento version as the file name, extract it, and move the SQL file into `.docker/mysql/docker-entrypoint-initdb.d/` and the PHP file into `app/etc/`

-  Run `docker-compose up -d` to bring up the containers in the background

-  Run `docker-compose run deploy cloud-deploy` and `docker-compose run deploy cloud-post-deploy` to run the deploy and post-deploy tasks

-  Run `docker-compose run deploy bin/magento indexer:reindex` to set up the initial indexes

-  Run `docker-compose run deploy bin/magento deploy:mode:set developer` to enable developer mode

-  Open https://magento2.docker/ and https://magento2.docker/admin to access the Magento storefront and admin panel
